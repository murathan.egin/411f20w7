const express = require('express')
const expressGraphQL = require('express-graphql')
const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString
} = require('graphql')
const app = express()

const authors = [
    { id: 1, name: 'Robert Galbraith' },
    { id: 2, name: 'J. R. R. Tolkien' },
    { id: 3, name: 'Leo Tolstoy' }
]

const books = [
    { id: 1, name: 'The Silkworm', authorId: 1},
    { id: 2, name: 'Career Of Evil', authorId: 1},
    { id: 3, name: 'Lethal Evil', authorId: 1},
    { id: 4, name: 'The Hobbit', authorId: 2},
    { id: 5, name: 'The Silmarillion', authorId: 2},
    { id: 6, name: 'Anna Karenina', authorId: 3},
    { id: 7, name: 'War And Peace', authorId: 3}
]

const RootQueryType = new GraphQLObjectType({
    name: 'Query',
    description: 'Root Query', 
    fields: () => ({
        
    })
})

app.use('/graphql', expressGrahQL({
    schema: schema, 
    grahiql: true
}))
app.listen(5000., () => console.log('Server Running'))